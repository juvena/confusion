var app = angular.module('confusionApp');
        app.controller('MenuController', ['$scope', 'menuFactory',function($scope, menuFactory){
            $scope.tab = 1;
            $scope.filtText = '';
            $scope.showDetails = false;
            $scope.dishes = menuFactory.getDishes();

            $scope.isSelected = function(tabId){
                return ($scope.tab === tabId);};
            
            $scope.select = function(setTab) {
                $scope.tab = setTab;

                if (setTab === 2)
                    $scope.filtText = "appetizer";
                else if (setTab === 3)
                    $scope.filtText = "mains";
                else if (setTab === 4)
                    $scope.filtText = "dessert";
                else
                    $scope.filtText = "";
            };
            $scope.toggleDetails = function() {
                $scope.showDetails = !$scope.showDetails;
            };

        }]);

        app.controller('ContactController', ['$scope', function($scope) {

            $scope.feedback = {mychannel:"", firstName:"", lastName:"",
                               agree:false, email:"" };
            var channels = [{value:"tel", label:"Tel."}, {value:"Email",label:"Email"}];
            $scope.channels = channels;
            $scope.invalidChannelSelection = false;
        }])

        app.controller('FeedbackController', ['$scope', function($scope) {
             $scope.sendFeedback = function() {
                console.log($scope.feedback);
                if ($scope.feedback.agree && ($scope.feedback.mychannel == "")&& !$scope.feedback.mychannel){
                    $scope.invalidChannelSelection = true;
                    console.log('incorrect');
                }else{
                        $scope.invalidChannelSelection = false;
                        $scope.feedback = {mychannel:"", firstName:"", lastName:"",
                                        agree:false, email:"" };
                        $scope.feedback.mychannel="";

                        $scope.feedbackForm.$setPristine();
                        console.log($scope.feedback);
                }
            };
        }]);

        app.controller('detail', ['$scope', 'menuFactory', function($scope, menuFactory) {
            

            var dish = menuFactory.getDish(3);
            $scope.dish = dish;

        }]);

        app.controller('CommentController', ['$scope', 'menuFactory' , function($scope, menuFactory){
            $scope.mycomment = {
                rating: 5,
                comment: "",
                author: "",
                date: ""
            };
            $scope.submitComment = function () {
                $scope.mycomment.date = new Date().toISOString();
                //$scope.mycomment.date = Date.now();
                $scope.commentForm.$setPristine();
                $scope.dish = menuFactory.getDish(3);
                console.log($scope.mycomment);
                $scope.dish.comments.push($scope.mycomment);
                $scope.mycomment = {
                    rating: 5,
                    comment: "",
                    author: "",
                    date: ""
                };
            };

        }]);